package com.example.springaop.service;

import com.example.springaop.aspect.Logging;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Logging
    public void assist(){
        System.out.println(methodName());
    }

    @Logging
    public void assist(String str){
        System.out.println(methodName() + " -> " + str);
    }

    private String methodName(){
        StackWalker.StackFrame stackFrame = StackWalker.getInstance()
                .walk(s -> s.skip(1).findFirst())
                .get()
                ;
        return stackFrame.getFileName() + " -> " + stackFrame.getMethodName();
    }
}
