package com.example.springaop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;


/**
 * Pointcut - @Before, @AfterReturning, @AfterThrowing, @Around
 * Pointcut exp
 * - whatever written inside Pointcut annotation
 * - it helps aspect to locate the target method(s), where this pointcut is applicable.
 *   e.g.
 *   1. execution( * com.example.springaop.service.StudentService.assist(..))
 *    - first * means any return type
 *    - .. means any arguments
 *
 *   2. execution( * com.example.springaop.*.*.assist(..))
 *    - first * means any return type
 *    - second * means any sub package of com.example.springaop
 *    - third * means any class
 *    - .. means any arguments
 */
@Aspect
@Component
public class LoggingAspect {

    /*
    this helps us to define point exp for method level
     */
    final String pointcutExpWay1 = "execution( * com.example.springaop.service.StudentService.assist(..))";

    /*
    this helps us to define point exp for class level
     */
    @Pointcut(value = "within(com.example.springaop.service.StudentService)")
    private void pointcutExpWay2(){

    }

    // WORKING
    // @Before(value = "pointcutExpWay1")
    // Commented @Before because we are using @Around
    // @Before(value = "pointcutExpWay2()")
    public void before(JoinPoint joinPoint){
        System.out.println(joinPoint.getSignature());
        System.out.println("LoggingAspect -> before");
    }

    // WORKING
    // Commented @After because we are using @Around
    // @After(value = "execution( * com.example.springaop.*.*.assist(..))")
    public void after(){
        System.out.println("LoggingAspect -> after");
    }

    //@Around(value = pointcutExpWay1)
    //@Around(value = "pointcutExpWay2()")
    @Around(value = "@annotation(Logging)")
    public Object around(ProceedingJoinPoint joinPoint){
        System.out.println("-------------------Start--------------");
        Object response = null;
        try {
            // @Before code start
            before(joinPoint);
            // @Before code end
            response = joinPoint.proceed();
            // @AfterReturning code here
        } catch (Throwable e) {
            // @AfterThrowing code here
            e.printStackTrace();
        }finally {
            // @After code  start
            after();
            // @After code  end
        }
        System.out.println("-------------------End--------------");
        return response;
    }
}
