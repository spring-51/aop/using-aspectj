package com.example.springaop;

import com.example.springaop.config.BeanConfig;
import com.example.springaop.service.StudentService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringAopApplication {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);
        StudentService studentService = context.getBean(StudentService.class);
        studentService.assist();
        studentService.assist("test");
    }

}
