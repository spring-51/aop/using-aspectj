# Integrating AOP with Spring Context

## Steps

```
s1 - Add below dependencies
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjrt</artifactId>
    <version>1.9.7</version>
</dependency>
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.9.7</version>
</dependency>

s2 - Add @EnableAspectJAutoProxy on any Config class

s3 - Define Aspect class
   - refer LoggingAspect

s4 -  add any Stereotype annotation on aspect class
   - refer LoggingAspect

s5 - define Aspect advices in Aspect class
   -  refer - LoggingAspect -> before()

```

### Ways of defining pointcuts
```
Way 1
  - using - execution
  - "execution( * com.example.springaop.service.StudentService.assist(..))";
  -  refer - LoggingAspect -> before()
Way 2 
 - using - within
 - e.g. @Pointcut(value = "within(com.example.springaop.service.StudentService)")
 - refer - LoggingAspect -> pointcutExpWay2()
 - refer - LoggingAspect -> before()

Way 3 
 - using - @annotation
  - refer - https://www.baeldung.com/spring-aop-annotation
  - refer - Logging
  - refer - LoggingAspect -> around
  USAGE
  - refer - StudentService - assist()
  - refer - StudentService - assist(String)
```